*************
Auction House
*************

Sample ETL (Extract-Transform-Load) application using Python and its awesome libraries.

Getting Started
***************

We use `pipenv <https://docs.pipenv.org/>` to manage project development environment.
To setup project, run: 

::
  pipenv install --dev


This will create new python virtualenv and download all dependencies.
After everything is setup, you can start your virtualenv by running ``pipenv shell`` or start application by ``pipenv run python auction-house/app.py``.

Contributors
************

* Jan Ferko
